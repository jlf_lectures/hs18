package ch.hepia.tpscala


/* Implémentez les fonctions suivantes. 
 */
object Collect {

  case class Album( title: String, artist: String, year: Int )
  case class Duration( minutes: Int, seconds: Int )
  case class Track( title: String, duration: Duration )

  val albums = List(
    Album( "Mit Gas", "Tomahawk", 2003 ),
    Album( "Pork Soda", "Primus", 1993 ),
    Album( "Brown Album", "Primus", 1997 ),
    Album( "Distraction Pieces", "Scroobius Pip", 2011 )
  )

  val tracks = Map(
    "Mit Gas" -> List(
      Track( "Mayday", Duration( 3, 32 ) )
    ),
    "Pork Soda" -> List(
      Track( "DMV", Duration( 4, 58 ) ),
      Track( "Mr. Krinkle", Duration( 5, 27 ) )
    ),
    "Brown Album" -> List(
      Track( "Fisticuffs", Duration( 4, 25 ) ),
      Track( "Camelback Cinema", Duration( 4, 0 ) ),
      Track( "Kalamazoo", Duration( 3, 31 ) )
    ),
    "Distraction Pieces" -> List(
      Track( "Let 'Em Come", Duration( 4, 25 ) ),
      Track( "Domestic Silence", Duration( 3, 58 ) )
    )
  )

  /* Retourne la liste de morceaux associés à un artiste */
  /*def tracksOf( artist: String ): List[Track] =
    albums
      .filter(_.artist == artist)
      .flatMap{ alb =>
        tracks( alb.title)
      }*/
  def tracksOf( artist: String ): List[Track] =
    for {
      alb <- albums if alb.artist == artist
      track <- tracks( alb.title )
    } yield track
   


  private def duration2seconds( d: Duration )  = d.minutes*60 + d.seconds
  /* Retourne la liste de tous les morceaux de moins de 4 minutes */
  /*def shortTracks: List[Track] = tracks.toList.flatMap{
    case (_,ts) => ts
  }.filter( t => duration2seconds(t.duration) < 60*4 )*/

  def shortTracks: List[Track] =
    for {
      (_,ts) <- tracks.toList
      track <- ts
      if duration2seconds(track.duration) < 60*4 
    } yield track



  /* Retourne les titres des morceaux antérieurs à une année */
  /*def titlesBefore( year: Int ): List[String] =
    albums
      .filter( _.year < year )
      .map( _.title )
      .flatMap( tracks )
   .map( _.title )*/
  def titlesBefore( year: Int ): List[String] =
    for {
      alb <- albums if alb.year < year
      track <- tracks( alb.title )
    } yield track.title
  

  /* Calcule la durée totale de tous les morceaux disponibles.
     REMARQUE: ont veut que les secondes soient inférieures à 60 mais les
     minutes peuvent dépasser ce total.
   */
  /*def totalDuration: Duration = {
    val s = tracks.toList.flatMap{ case (_,ts) =>
      ts.map( t => duration2seconds(t.duration) )
    }.sum
    val min = s / 60
    val sec = s % 60
    Duration(min,sec)
   }*/

  def totalDuration: Duration = {
    val durations = for {
      (_,ts) <- tracks.toList
      track <- ts
    } yield duration2seconds(track.duration)
    val total = durations.sum
    val min = total / 60
    val sec = total % 60
    Duration(min,sec)
  }




}
