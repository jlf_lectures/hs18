package ch.hepia.tpscala

/*
 * Implémenter les fonctions suivantes en suivant les commentaires.
 */

object Predicates {

  /*
   * La méthode 'not' retourne un nouveau prédicat dont le résultat
   * est toujours la négation du résultat de l'argument.
   */
  def not[A]( p: (A)=>Boolean ): (A)=>Boolean = { (a:A) =>
    !p(a)
  }

  /*
   * La méthode 'and' retourne un nouveau prédicat dont le résultat
   * est toujours la conjonction des résultats des deux arguments.
   */
  def and[A]( p1: (A)=>Boolean, p2: (A)=>Boolean ): (A)=>Boolean = { (a:A) =>
    p1(a) && p2(a)
  }

  /*
   * La fonction 'or' retourne un nouveau prédicat dont le résultat
   * est toujours la disjonction des résultats des deux arguments.
   */
  def or[A]( p1: (A)=>Boolean, p2: (A)=>Boolean ): (A)=>Boolean = { (a:A) =>
    p1(a) || p2(a)
  }

  val minor = (u:User) => u.age < 18
  val creditCard (u:User) => creditCardValidator.isValid(u.creditCard)
  val parentConsent = (u:User) => u.parentConsent

  val major = not( minor )
  val validBuyer = and( or( major, parentConsent), creditCard )

  /*
   * La fonction 'exists' retourne un nouveau prédicat dont le
   * résultat est vrai si au moins un des prédicat de l'argument est
   * vrai.
   */

  type Pred[A] = (A)=>Boolean
  type PredList[A] = List[Pred[A]]

  def exists[A]( ps: PredList[A] ): Pred[A] = { 
    def loop( elem: A, rest: PredList[A] ): Boolean = rest match {
      case Nil => false
      case p :: _ if p(elem) => true
      case _ :: tail => loop(elem,tail)
    }
    (a:A) => loop(a,ps)
  }

  /*
   * La fonction 'forall' retourne un nouveau prédicat dont le
   * résultat est vrai si et seulement si tous les prédicats de
   * l'argument sont vrais.
   */
  def forall[A]( ps: PredList[A] ): Pred[A] = { (a:A) =>
    def loop( rest: PredList[A] ): Boolean = rest match {
      case Nil => true
      case p :: _ if !p(a) => false
      case _ :: tail => loop(tail)
    }
    loop(ps)
  }


}
