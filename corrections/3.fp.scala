
/*
 * Copiez ce fichier dans le répértoire 'src/' de votre projet sbt.
 * Implémenter les fonctions suivantes en suivant les commentaires.
 * Respectez également les consignes suivantes:
 *  - Toutes les fonctions doivent être pures
 *  - Tout doit être immutable (val au lieu de var)
 *  - Utilisez la recursion terminale si possible
 *  - Utilisez le pattern matching si possible
 */
object Serie3 {

  /*
   * Donne la longueur d'une liste. Votre implémentation ne peut
   *  utiliser aucune fonction de List excepté isEmpty()
   */
  def len[A]( as: List[A] ): Int = {
    def loop( rest: List[A], size: Int ): Int =
      rest match {
        case Nil => size
        case first :: subList => loop( subList, size+1 )
      }
    loop( as, 0 )
  }
    

  /*
   * Renversez d'une liste. Votre implémentation ne peut
   * utiliser aucune fonction de List excepté: -
   *    - isEmpty
   *    - ::
   *    - head
   *    - tail
   */
  def rev[A]( as: List[A] ): List[A] = {
    def loop( remaining: List[A], result: List[A] ): List[A] =
      if( remaining.isEmpty ) result
      else loop( remaining.tail, remaining.head :: result )
      /*remaining match {
        case Nil => result
        case el :: rest => loop( rest, el :: result )
      }*/
    loop( as, Nil )
  }

  //and( Nil ) == true
  //and( xs ++ ys ) == and(xs) && and(ys)
  //and( Nil ++ ys ) == and(Nil) && and(ys) == and(ys)

  def and( xs: List[Boolean] ): Boolean = {
    def loop( rest: List[Boolean] ): Boolean = rest match {
      case Nil => true
      case false :: _ => false
      case _ :: tail => loop(tail)
    }
    loop(xs)
  }

  /*
   *  Applatit une liste. Votre implémentation 
   *  ne peut utiliser aucune fonction de List excepté:
   *   - isEmpty
   *   - head
   *   - tail
   *   - ++
   */
  def flat[A]( las: List[List[A]] ): List[A] = {
    def loop( rest: List[List[A]], res: List[A] ): List[A] =
      rest match {
        case subList :: rest2 => loop( rest2, res ++ subList )
        case Nil => res
      }
    loop( las )
  }

  /*
   *  Retourne vrai si la liste contiens un nombre pair d'éléments
   *  Votre implémentation ne peut utiliser aucune fonction de
   *  List !  Vous devez utiliser le pattern matching.
   */
  def even[A]( as: List[A] ): Boolean = {
    def loop(rest: List[A] ): Boolean = 
      rest match  {
        case Nil => true
        case _ :: Nil => false
        case _ :: _ :: rest2 => loop(rest2)
      }
    loop(as)
  }

}

object Serie3Main extends App {

  import Serie3._

  val is = List( 1, 2, 3, 4, 5 )
  val bs1 = List( true, true, false, true )
  val bs2 = List( true, true, true )
  val las1 = List.empty[List[Int]]
  val las2 = List( List(1,2), List(3), List(4,5) )

  require( len(is) == 5 )
  require( len( las1 ) == 0 )
  require( len( bs1 ) == 4 )

  require( rev(is) == List( 5, 4, 3, 2, 1 ) )
  require( rev( bs1 ) == List( true, false, true, true ) )
  require( rev( bs2 ) == bs2 )

  require( sum(is) == 15 )
  require( sum(Nil) == 0 )

  require( flat(las1) == Nil )
  require( flat(las2) == is )

  require( and(bs1) == false )
  require( and(bs2) == true )

  require( even( is ) == false )
  require( even( bs1 ) == true )
  require( even( las1 ) == true )

}
