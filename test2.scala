def wat[A,B,C,D,E]( f: A=>C, g: B=>D, h: (C,D)=>E ): (A,B)=>E =
  (a,b) => h( f( a ) , g( b ) )


def size[A]( nel: NEL[A] ): Int = {
  def loop( rest: NEL[A], s: Int ): Int = rest match {
    case Last(_) => s
    case Cons( _, tail ) => loop( tail, s+1 )
  }
  loop( nel, 1 )
}


//NON TAIL REC
def build[A]( head: A, tail: List[A] ): NEL[A] = tail match {
  case Nil => Last(head)
  case h :: t => Cons( head, build( h, t ) )
}

//TAIL REC mais à l'envers
def build[A]( head: A, tail: List[A] ): NEL[A] = {
  def loop( rest: List[A], nel: NEL[A] ) = rest {
    case Nil => nel
    case h :: t => loop( t, Cons( h, nel ) )
  }
  loop( tail, Last( head ) )
}

//TAIL REC 
def build[A]( head: A, tail: List[A] ): NEL[A] = {
  def loop( rest: List[A], nel: NEL[A] ) = rest {
    case Nil => nel
    case h :: t => loop( t, Cons( h, nel ) )
  }
  val full = (head :: tail ).reverse
  loop( full.tail, Last( full.head ) )
}

//non tail rec
def map[A,B]( as: NEL[A] )( f: A=>B ): NEL[B] = as match {
  case Last(a) => Last(f(a))
  case Cons(a,rest) => Cons( f(a), map(rest)(f) )
}

//tail rec mais à l'envers
def map[A,B]( as: NEL[A] )( f: A=>B ): NEL[B] = {
  def loop( next: NEL[A], res: NEL[B] ): NEL[B] = next match {
    case Last(a) => Cons( f(a), res )
    case Cons(a,rest) => loop( rest, Cons( f(a), res ) )
  }
  as match {
    case Last(a) => Last(f(a))
    case Cons(a,tail) => loop( tail, Last(f(a)) )
  }
}

//tail rec mais juste
def mapReverse[A,B]( as: NEL[A] )( f: A=>B ): NEL[B] = {
  def loop( next: NEL[A], res: NEL[B] ): NEL[B] = next match {
    case Last(a) => Cons( f(a), res )
    case Cons(a,rest) => loop( rest, Cons( f(a), res ) )
  }
  as match {
    case Last(a) => Last(f(a))
    case Cons(a,tail) => loop( tail, Last(f(a)) )
  }
}

def reverse[A]( nel: NEL[A] ): NEL[A] = 
  mapReverse( nel )( identity )

def map[A,B]( as: NEL[A] )( f: A=>B ): NEL[B] =
  reverse( mapReverse( as )( f ) )

